@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-sm-offset-2 col-sm-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Edit Planet
                </div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    @include('errors.errors')
                    <form action="{{url('planet/store')}}/{{$planet->id}}" method="POST" class="form-horizontal">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="form-group">
                            <label for="planet-name" class="col-sm-3 control-label">Name:</label>
                            <div class="col-sm-6">
                                <input type="text" name="name" id="planet-name" class="form-control"
                                       value="{{$planet->name}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="planet-size" class="col-sm-3 control-label">Size</label>
                            <div class="col-sm-6">
                                <select class="form-control" name="size" id="planet-size">
                                    @for($i = 1; $i <= 20; $i++)
                                        @if($planet->size == $i)
                                            <option value="{{$i}}" selected>{{$i}}x{{$i}}</option>
                                        @else
                                            <option value="{{$i}}">{{$i}}x{{$i}}</option>
                                        @endif
                                    @endfor

                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-default">
                                    <i class="fa fa-btn fa-edit"></i>Edit Planet
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div id="selected-planet">
                </div>
                <div class="panel-heading">
                    Edit Terrain
                </div>

                <div class="panel-body form-inline">
                    <div class="form-group col-sm-2">
                        <label for="x-cord" class="control-label">X:</label>
                        <select class="form-control" name="x-cord" id="x-cord">
                            @for($i = 0; $i < $planet->size; $i++)
                                <option value="{{$i}}">{{$i}}</option>
                            @endfor
                        </select>
                    </div>
                    <div class="form-group col-sm-2">
                        <label for="y-cord" class="control-label">Y:</label>
                        <select class="form-control" name="y-cord" id="y-cord">
                            @for($i = 0; $i < $planet->size; $i++)
                                <option value="{{$i}}">{{$i}}</option>
                            @endfor
                        </select>
                    </div>
                    <div class="form-group col-sm-4">
                        <label for="terrain" class="ontrol-label">Terrain:</label>
                        <select class="form-control" name="terrain" id="terrain">
                            @foreach($terrains as $terrain)
                                <option value="{{$terrain->id}}">{{$terrain->long_desc}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-default" id="addButton">
                            <i class="fa fa-btn fa-plus"></i>Add
                        </button>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('script')
    <script>
        $(function () {
            var id = '{{$planet->id}}';
            var token = '{{csrf_token()}}';

            function updateMap() {
                $.ajax({
                    url: '/api/planet/' + id + '',
                    success: function (result) {
                        $('#selected-planet').html(result);
                    }
                });
                $.ajax({
                    url: '/api/planet/' + id + '/terrain',
                    success: function (result) {
                        $.each(result.planet_terrains, function (key, value) {
                            var id = '#' + value.x + '-' + value.y;
                            $(id).html("<img src='http://img.swcombine.com//galaxy/terrains/" + value.terrain.short_desc + "/terrain.gif' width ='40' title = '(" + value.x + "," + value.y + ") " + value.terrain.long_desc + "'> ");
                        })

                    }
                });
            }

            updateMap();

            $('#addButton').on('click', function () {
                $.ajax({
                    url: '/api/planet/' + id + '/edit',
                    method: 'POST',
                    data: {
                        '_token': token,
                        'id': id,
                        'x': $('#x-cord').val(),
                        'y': $('#y-cord').val(),
                        'terrain_id': $('#terrain').val()
                    },
                    success: function (result) {
                        console.log(result);
                    }
                });
                updateMap();
            })
        });
    </script>


@endsection