@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome</div>

                <div class="panel-body">
                    SWC Prospect Manager App - for demo use only.
                    <p>Features are incomplete and not designed for use. There is no test coverage for the code - data loss may result. Use at own risk.</p>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
