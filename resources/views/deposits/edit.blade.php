@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-sm-offset-2 col-sm-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Current Deposits
                </div>
                <div class="panel-body" id="deposit-list">

                </div>

                <div id="selected-planet">
                </div>
                <div class="panel-heading">
                    Add/Change Deposits
                </div>

                <div class="panel-body form-inline">
                    <div class="form-group">
                        <label for="x-cord" class="control-label">X:</label>
                        <select class="form-control" name="x-cord" id="x-cord">
                            @for($i = 0; $i < $planet->size; $i++)
                                <option value="{{$i}}">{{$i}}</option>
                            @endfor
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="y-cord" class="control-label">Y:</label>
                        <select class="form-control" name="y-cord" id="y-cord">
                            @for($i = 0; $i < $planet->size; $i++)
                                <option value="{{$i}}">{{$i}}</option>
                            @endfor
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="type" class="control-label">Type:</label>
                        <select class="form-control" name="type" id="type">
                            @foreach($types as $type)
                                <option value="{{$type->id}}">{{$type->type_desc}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="quantity" class="control-label">Quantity:</label>
                        <input class="form-control" type="number" min="0" name="quantity" id="quantity" required>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-default" id="addButton">
                            <i class="fa fa-btn fa-plus"></i>Add
                        </button>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('script')
    <script>
        $(function () {

            var id = '{{$planet->id}}';
            var token = '{{csrf_token()}}';

            $.ajax({
                url: '/api/planet/' + id + '',
                success: function (result) {
                    $('#selected-planet').html(result);
                }
            });

            $.ajax({
                url: '/api/planet/' + id + '/terrain',
                success: function (result) {
                    $.each(result.planet_terrains, function (key, value) {
                        var id = '#' + value.x + '-' + value.y;
                        $(id).html("<img src='http://img.swcombine.com//galaxy/terrains/" + value.terrain.short_desc + "/terrain.gif' width ='40' title = '(" + value.x + "," + value.y + ") " + value.terrain.long_desc + "'> ");
                    })

                }
            });

            function updateDeposits(){
                $.ajax({
                    url: '/api/deposit/' + id + '',
                    success: function (result) {
                        $('#deposit-list').html(result);
                    }
                });
                $.ajax({
                    url: '/api/deposit/' + id + '/raw',
                    success: function(result){
                        console.log(result);
                        $.each(result.deposits, function (key, value) {
                            var id = '#' + value.x + '-' + value.y;
                            $(id).html("<img src='http://img.swcombine.com/materials/" + value.type_id + "/main.jpg' width ='40' title = '(" + value.x + "," + value.y + ") " + value.type.type_desc + "' class='img-overlay'>");
                        })
                    }
                })
            }
            updateDeposits();

            $('#addButton').on('click', function () {
                $.ajax({
                    url: '/api/deposit/' + id + '/edit',
                    method: 'POST',
                    data: {
                        '_token': token,
                        'id': id,
                        'x': $('#x-cord').val(),
                        'y': $('#y-cord').val(),
                        'type_id': $('#type').val(),
                        'quantity': $('#quantity').val()
                    },
                    success: function (result) {
                    }
                });
                updateDeposits();
            })
        });
    </script>


@endsection