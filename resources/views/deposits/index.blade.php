@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="col-sm-offset-2 col-sm-8">
            <!-- Current planets -->
            @if (count($planets) > 0)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        List of Planets
                    </div>

                    <div class="panel-body">
                        <table class="table table-striped planet-table">
                            <thead>
                            <th>Planet Name</th>
                            <th>Planet Size</th>
                            <th>&nbsp;</th>
                            </thead>
                            <tbody>
                            @foreach ($planets as $planet)
                                <tr id="{{$planet->id}}" class="planet">
                                    <td class="table-text">
                                            <div>{{ $planet->name }}</div>
                                    </td>
                                    <td class="table-text">
                                        <div>{{ $planet->size }} x {{ $planet->size }}</div>
                                    </td>
                                    <td>
                                        <form action="/deposit/{{ $planet->id }}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('PUT') }}

                                            <button type="submit" id="edit-planet-{{ $planet->id }}" class="btn">
                                                <i class="fa fa-btn fa-edit"></i>Edit Deposits
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif
           <div id="selected-planet">

           </div>

        </div>
    </div>
@endsection

@section('script')
    <script>
        $(function(){
            $('.planet').on('click', function(){
                var id = $(this).attr('id');
                $.ajax({
                    url: 'api/planet/'+id+'',
                    success: function(result){
                        $('#selected-planet').html(result);
                    }
                });
                $.ajax({
                    url: '/api/planet/' + id + '/terrain',
                    success: function (result) {
                        $.each(result.planet_terrains, function (key, value) {
                            var id = '#' + value.x +'-' + value.y;
                            $(id).html("<img src='http://img.swcombine.com//galaxy/terrains/" + value.terrain.short_desc + "/terrain.gif' width ='40' title = '("+value.x+","+value.y+") "+value.terrain.long_desc+"'> ");
                        })

                    }
                });
            });
        });
    </script>
@endsection