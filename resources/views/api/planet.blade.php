<link href="{{ URL::asset('css/planet_grid.css') }}" rel="stylesheet">
<div class="panel panel-default">
    <div class="panel-heading">
        {{$planet->name}}
    </div>

    <div class="panel-body">
        <table id="grid" class="grid" border="1">
            <tbody>
            @for($i = 0; $i < $planet->size; $i++)
                <tr>
                    @for($j = 0; $j < $planet->size; $j++)
                        <td id="{{$j}}-{{$i}}" title="({{$j}}, {{$i}})"></td>
                    @endfor
                </tr>
            @endfor
            </tbody>
        </table>
    </div>
</div>