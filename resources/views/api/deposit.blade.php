<table class="table table-striped deposit-table">
    <thead>
    <th>&nbsp;</th>
    <th>Deposit Name</th>
    <th>Location</th>
    <th>Quantity</th>
    <th>&nbsp;</th>
    </thead>
    <tbody>
    @foreach ($deposits as $deposit)
        <tr>
            <td>
                <img src="http://img.swcombine.com//materials/{{$deposit->type_id}}/main.jpg" width="100px" />
            </td>
            <td class="table-text">
                <div>{{ $deposit->type->type_desc }}</div>
            </td>
            <td class="table-text">
                <div>({{ $deposit->x }} , {{ $deposit->y }})</div>
            </td>
            <td class="table-text">
                <div>{{$deposit->quantity}} Units</div>
            </td>
            <td>
                <form action="/deposit/{{ $deposit->id }}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}

                    <button type="submit" id="delete-deposit-{{ $deposit->id }}"
                            onclick="return confirm('Are you sure you want to delete?');"
                            class="btn btn-danger">
                        <i class="fa fa-btn fa-trash"></i>Delete
                    </button>
                </form>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>