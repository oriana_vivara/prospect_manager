<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Planet
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $name
 * @property integer $size
 * @method static \Illuminate\Database\Query\Builder|\App\Planet whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Planet whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Planet whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Planet whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Planet whereSize($value)
 */
class Planet extends Model
{
    //
}
