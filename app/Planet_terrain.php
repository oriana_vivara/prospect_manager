<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Planet_terrain
 *
 * @property integer $id
 * @property integer $terrain_id
 * @property integer $planet_id
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $created_at
 * @method static \Illuminate\Database\Query\Builder|\App\Planet_terrain whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Planet_terrain whereXCord($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Planet_terrain whereYCord($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Planet_terrain whereTerrainId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Planet_terrain wherePlanetId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Planet_terrain whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Planet_terrain whereCreatedAt($value)
 * @mixin \Eloquent
 * @property integer $x
 * @property integer $y
 * @method static \Illuminate\Database\Query\Builder|\App\Planet_terrain whereX($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Planet_terrain whereY($value)
 * @property-read \App\Terrain $terrain
 */
class Planet_terrain extends Model
{
    public function terrain(){
        return $this->belongsTo('App\Terrain');
    }
}
