<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Deposit
 *
 * @property integer $id
 * @property integer $planet_id
 * @property integer $x
 * @property integer $y
 * @property integer $type_id
 * @property integer $quantity
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Type[] $type
 * @method static \Illuminate\Database\Query\Builder|\App\Deposit whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Deposit wherePlanetId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Deposit whereX($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Deposit whereY($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Deposit whereTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Deposit whereQuantity($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Deposit whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Deposit whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Deposit extends Model
{
    public function type(){
        return $this->belongsTo('App\Type');
    }
}
