<?php

namespace App\Http\Controllers;

use App\Deposit;
use Illuminate\Http\Request;
use App\Planet;
use App\Planet_terrain;
use App\Terrain;

use App\Http\Requests;

class apiController extends Controller
{
    public function getPlanet(REQUEST $request)
    {
        $planet = Planet::find($request->id);
        $terrains = Terrain::all();


        $response = ['planet' => $planet, 'terrains' => $terrains];


        return view('api.planet', $response);
    }

    public function getDeposits(REQUEST $request)
    {
        $deposits = Deposit::where('planet_id', $request->id)->with('type')->get();


        return view('api.deposit', ['deposits' => $deposits]);
    }

    public function editTerrain(REQUEST $request)
    {
        //check if the terrain already exists
        try {
            $planet_terrain = Planet_terrain::where('x', $request->x)->where('y', $request->y)->where('planet_id', $request->id)->firstOrFail();
        } catch (\Exception $e) {
            //does not exist
            $planet_terrain = new Planet_terrain();
        }

        $planet_terrain->x = $request->x;
        $planet_terrain->y = $request->y;
        $planet_terrain->terrain_id = $request->terrain_id;
        $planet_terrain->planet_id = $request->id;

        $planet_terrain->save();

    }

    public function getTerrain(REQUEST $request)
    {
        $planet_terrains = Planet_terrain::where('planet_id', $request->id)->with('terrain')->get();
        return response()->json(['planet_terrains' => $planet_terrains]);
    }

    public function editDeposit(REQUEST $request)
    {
        //check if the deposit already exists
        try {
            $deposit = Deposit::where('x', $request->x)->where('y', $request->y)->where('planet_id', $request->id)->firstOrFail();
        } catch (\Exception $e) {
            //does not exist
            $deposit = new Deposit();
        }

        $deposit->x = $request->x;
        $deposit->y = $request->y;
        $deposit->type_id = $request->type_id;
        $deposit->quantity = $request->quantity;
        $deposit->planet_id = $request->id;

        $deposit->save();

    }

    public function getDepositsJson(REQUEST $request)
    {
        $deposits = Deposit::where('planet_id', $request->id)->with('type')->get();
        return response()->json(['deposits' => $deposits]);
    }

    public function deleteDeposit(Request $request)
    {
        $deposit = Deposit::find($request->id);
        $deposit->delete();
        return true;
    }
}
