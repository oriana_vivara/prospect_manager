<?php

namespace App\Http\Controllers;

use App\Type;
use Illuminate\Http\Request;
use App\Planet;
use App\Terrain;
use App\Deposit;

use App\Http\Requests;

class depositController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $planets = Planet::all();
        $terrains = Terrain::all();

        return view('deposits.index', ['planets' => $planets, 'terrains' => $terrains]);
    }

    public function edit(REQUEST $request){
        $planet = Planet::find($request->id);
        $types = Type::all();
        return view('deposits.edit', ['planet' => $planet, 'types' => $types]);
    }

    public function delete(Request $request){
        $deposit = Deposit::find($request->id);
        $deposit->delete();
        return redirect('/deposit');
    }


}
