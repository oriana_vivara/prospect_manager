<?php

namespace App\Http\Controllers;

use App\Planet;
use App\Terrain;
use App\Planet_terrain;
use Illuminate\Http\Request;

use App\Http\Requests;

class planetController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $planets = Planet::all();
        $terrains = Terrain::all();

        return view('planets.index', ['planets' => $planets, 'terrains' => $terrains]);
    }

    public function storePlanet(Request $request){
        $this->validate($request, [
            'name' => 'required|max:45',
            'size' => 'required'
        ]);

        $planet = new Planet();
        $planet->name = $request->name;
        $planet->size = $request->size;
        $planet->save();

        return redirect('/planet');
    }

    public function editPlanet(Request $request){
        $this->validate($request, [
            'name' => 'required|max:45',
            'size' => 'required'
        ]);

        $planet = Planet::find($request->id);
        $planet->name = $request->name;
        $planet->size = $request->size;
        $planet->save();

        return redirect()->back();
    }

    public function delete(Request $request){
        $planet = Planet::find($request->id);
        $planet->delete();
        return redirect('/planet');
    }

    public function edit(REQUEST $request){
        $planet = Planet::find($request->id);
        $terrains = Terrain::all();
        $planet_terrains = Planet_terrain::where('planet_id', $request->id);
        return view('planets.edit', ['planet' => $planet, 'terrains' => $terrains, 'planet_terrains' => $planet_terrains]);
    }


}
