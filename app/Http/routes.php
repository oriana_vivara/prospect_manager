<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::get('/', function () {
        return view('welcome');
    });

    Route::auth();
    Route::group(['prefix' => 'planet'], function () {
        Route::get('/', 'planetController@index');
        Route::put('{id}', 'planetController@edit');
        Route::delete('{id}', 'planetController@delete');
        Route::get('{id}', 'planetController@edit');

        Route::post('store', 'planetController@storePlanet');
        Route::put('store/{id}', 'planetController@editPlanet');

    });

    Route::group(['prefix' => 'deposit'], function(){
        Route::get('/', 'depositController@index');
        Route::put('{id}', 'depositController@edit');
        Route::delete('{id}', 'depositController@delete');
    });


    Route::group(['prefix' => 'api'], function () {
        Route::get('/planet/{id}', 'apiController@getPlanet');
        Route::post('/planet/{id}/edit', 'apiController@editTerrain');
        Route::get('/planet/{id}/terrain', 'apiController@getTerrain');

        Route::get('/deposit/{id}', 'apiController@getDeposits');
        Route::post('/deposit/{id}/edit', 'apiController@editDeposit');
        Route::get('/deposit/{id}/raw', 'apiController@getDepositsJson');

    });

});
