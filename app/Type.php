<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Type
 *
 * @property integer $id
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $created_at
 * @property string $type_desc
 * @property-read \App\Deposit $deposit
 * @method static \Illuminate\Database\Query\Builder|\App\Type whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Type whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Type whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Type whereTypeDesc($value)
 * @mixin \Eloquent
 */
class Type extends Model
{
    public function deposit(){
        return $this->hasMany('App\Deposit');
    }
}
