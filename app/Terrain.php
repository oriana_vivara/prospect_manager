<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Terrain
 *
 * @property integer $id
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $created_at
 * @property string $short_desc
 * @property string $long_desc
 * @method static \Illuminate\Database\Query\Builder|\App\Terrain whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Terrain whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Terrain whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Terrain whereShortDesc($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Terrain whereLongDesc($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Planet_terrain[] $planet_terrain
 */
class Terrain extends Model
{
    public function planet_terrain(){
        return $this->hasMany('App\Planet_terrain');
    }
}
